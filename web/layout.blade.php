<!doctype html>
<html lang="en">
    <head>
        <title>Stick</title>

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    </head>
    <body>
    <div class="d-flex flex-column" style="min-height:100vh">
        <nav class="navbar  navbar-expand-lg navbar-dark bg-dark mb-3">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/test">Test</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/test/test2">Test 2</a>
                </li>
            </ul>
        </nav>

        <div class="mb-3 pl-3 pr-3">
            @yield('content')
        </div>

        <footer class="mt-auto p-3 bg-light">
            <time>Request time: {{ microtime(true) - STICK_START_TIME }}</time>
        </footer>
    </div>
    </body>
</html>

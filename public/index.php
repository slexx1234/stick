<?php

define('STICK_START_TIME', microtime(true));
define('STICK_BASE_DIR', dirname(__DIR__, 1));
define('STICK_LOG_TO', STICK_BASE_DIR . '/stick.log');
define('STICK_LOG_LEVEL', 5);

try {
    $core = STICK_BASE_DIR . '/Stick.php';
    if (!file_exists($core)) {
        throw new \Exception('Твою мать куда дел класс палки?');
    }
    require_once $core;

    $autoload = STICK_BASE_DIR. '/vendor/autoload.php';
    if (!file_exists($autoload)) {
        throw new \Exception('1. Установи компосер https://getcomposer.org/. 2. Сделай в консоли composer install!');
    }
    require_once $autoload;
    
    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();

    Stick::load($_GET, $_POST, $argv);
} catch(\Exception $e) {
    file_put_contents(STICK_LOG_TO, 'ERROR: ' . $e->getFile() . '[' . $e->getLine() . '] >>> ' . $e->getMessage() . "\n");

    if (class_exists('Stick')) {
        if (Stick::isDebug() && class_exists('\Whoops\Run')) {
            $whoops = new \Whoops\Run;
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
            echo $whoops->handleException($e);
        } else {
            Stick::abort(500);
        }
    }
}

<?php

class Stick
{
    /**
     * @var bool
     */
    protected static $isLoaded = false;

    /**
     * @var null
     */
    protected static $config = [];

    /**
     * @var array
     */
    protected static $get = [];

    /**
     * @var array
     */
    protected static $post = [];

    /**
     * @var array
     */
    protected static $args = [];

    /**
     * Получение переменной `POST` запроса
     * @param string $key - Название переменной запроса
     * @param mixed [$default] - Значение по умолчанию, в слуае отсуцтвия переменной
     * @return mixed
     */
    public static function post(string $key, $default = null)
    {
        return self::$post[$key] ?? $default;
    }

    /**
     * Получение переменной `GET` запроса
     * @param string $key - Название переменной запроса
     * @param mixed [$default] - Значение по умолчанию, в слуае отсуцтвия переменной
     * @return mixed
     */
    public static function get(string $key, $default = null)
    {
        return self::$get[$key] ?? $default;
    }

    /**
     * Получение аргумента коммандной строки
     * @param int $i - Номер аргумента
     * @param mixed [$default] - Значение по умолчанию, в слуае отсуцтвия номера
     * @return mixed
     */
    public static function arg(int $i, $default = null)
    {
        return self::$args[$i] ?? $default;
    }

    /**
     * Запуск палки
     * @param array $get - Аргументы get запроса
     * @param array $post - Аргументы post запроса
     * @param array $args - Аргументы консоли
     * @return void
     * @throws Exception - Если палка уже запущена
     */
    public static function load($get, $post, $args)
    {
        if (self::isLoaded()) {
            throw new \Exception('Палка уже запущена!');
        }

        self::$get = $get;
        self::$post = $post;
        self::$args = $args;

        self::loadConfig();

        if (self::isCli()) {
            $file = STICK_BASE_DIR . '/cli/' . self::arg(1, 'index') . '.php';

            if (file_exists($file)) {
                include $file;
            } else {
                echo 'Command not found!';
                exit(255);
            }
        } else {
            $matched = explode('/', explode('?', $_SERVER['REQUEST_URI'])[0] ?? '');
            $fileOne = STICK_BASE_DIR . '/web/' . implode('/', $matched) . '/index.blade.php';
            $fileTwo = STICK_BASE_DIR . '/web/' . implode('/', $matched) . '.blade.php';

            if (file_exists($fileOne)) {
                echo self::view(implode('.', $matched) . '.index');
            } else if (file_exists($fileTwo)) {
                echo self::view(implode('.', $matched));
            } else {
                self::abort(404);
            }
        }

        self::$isLoaded = true;
    }

    /**
     * Функция проверяет запущена ли палка
     * @return bool
     */
    public static function isLoaded(): bool
    {
        return self::$isLoaded;
    }

    /**
     * Запись в лог
     * @param string $message - Сообщение которое будет записано в лог файл
     * @param int [$level] - Уровень логирования
     */
    public static function log(string $message, $level = 1)
    {
        @file_put_contents(STICK_LOG_TO, $message . "\n");
    }

    /**
     * Загрузка конфигурации
     * @throws Exception - Если нет конфига
     */
    protected static function loadConfig()
    {
        $configFile = STICK_BASE_DIR . '/config.ini';
        if (!file_exists($configFile)) {
            throw new \Exception('Куда бледь дел конфиг? Давай ищи! Он должен лежать здесь: "' . $configFile . '"');
        }

        self::$config = parse_ini_file($configFile);

        // Установка всякой херни по умолчанию
        date_default_timezone_set(self::config('timezone', 'Europe/Moscow'));
    }

    /**
     * Получение переменной конфига
     * @param string $key - Ключ конфигурации
     * @param mixed [$default] - Значение по умолчанию, в слуае отсуцтвия ключа
     * @return mixed
     */
    public static function config(string $key, $default = null)
    {
        return self::$config[$key] ?? $default;
    }

    /**
     * Проверяет включен ли дебаг режим
     * @return bool
     */
    public static function isDebug(): bool
    {
        return (bool) self::config('debug', !self::isLoaded());
    }

    /**
     * Проверяет запущена ли палка в режиме `cli`
     * @return bool
     */
    public static function isCli(): bool
    {
        return php_sapi_name() == 'cli';
    }

    /**
     * Проверяет запущена ли палка в режиме `web`
     * @return bool
     */
    public static function isWeb(): bool
    {
        return !self::isCli();
    }

    /**
     * Вывод вьюшки
     * @param string $name - Имя вьюшки
     * @return string
     */
    protected static function view(string $name): string
    {
        return (new \Philo\Blade\Blade( __DIR__ . '/web', __DIR__ . '/cache'))
            ->view()
            ->make($name)
            ->render();
    }



    /**
     * Вывод страницы ошибки сервера
     * @return void
     */
    public static function internalServerError()
    {
        self::abort(500);
    }

    /**
     * Вывод страницы ошибки 404 (страница не найдена)
     * @return void
     */
    public static function notFound()
    {
        self::abort(404);
    }

    /**
     * Выводт страницу для ошибки http
     * @param int $code - HTTP код состояния
     * @return void
     */
    public static function abort(int $code)
    {
        http_response_code($code);

        if (class_exists('\Philo\Blade\Blade')) {
            echo self::view($code);
            exit(0);
        } else {
            echo '<h1>500 Internal Server Error!</h1>';
            exit(255);
        }
    }

    /**
     * Крависый дамп переменной
     * @param mixed $var - Переменная для дампа
     * @return void
     */
    public static function dump($var)
    {
        dump($var);
        exit(0);
    }
}
